# module-graph-webpack-plugin #

A webpack plugin for generating a GraphML document containing modules and their dependencies.

# Installation #


```
#!bash

npm install --save-dev module-graph-webpack-plugin
```


# Usage #

In your webpack config file:

```
#!javascript

const ModuleGraphPlugin = require('module-graph-webpack-plugin');

module.exports = {
    plugins: [
        new ModuleGraphPlugin({
            filename: 'module-graph.graphml',
            use_groups: true
        })
    ]
};
```

# Configuration #

The plugin constructor accepts an optional configuration object.

The object must contain these properties:

* ```filename: string``` - the name of the GraphML file to write in the output directory.

The object may also contain these properties:

* ```use_groups: boolean``` whether to write sub-graphs into the output based on used module names. If false, the module nodes will not be grouped at all.