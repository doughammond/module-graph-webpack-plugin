const path = require('path');

/**
 * Deconstruct absolute module paths and return [package, filename] as a 2-element array
 * @param  {String} cwd    : current working directory name
 * @param  {String} name   : module name
 * @return {Array<String>} : 2-element array of strings
 */
const group_name = (cwd, resource) => {
  const m_path = path.relative(cwd, resource);
  const m_path_parts = m_path.split(path.sep);
  if (m_path_parts.length > 1 && m_path_parts[0] === 'node_modules') {
    m_path_parts.shift();
  }
  return [
    m_path_parts.slice(0,1)[0],
    m_path_parts.slice(1).join(path.sep)
  ];
};

/**
 * Make a GraphML <node> element string from module object m, using module name
 * @param  {Object} m    : webpack module object
 * @param  {String} name : exported module name
 * @return {String}      : string representation of GraphML <node> element
 */
const gml_node = (m ,name) => `
          <node id="${m.id}">
            <data key="d0"><![CDATA[${name}]]></data>
            <data key="d1">${m.size()/1024}</data>
            <data key="d2">${m.size()}</data>
          </node>`;

/**
 * Make a GraphML <edge> element string from module reason object r and module object m
 * @param  {Object} r : module.reason object
 * @param  {Object} m : module object
 * @return {String}   : string representation of a GraphML <edge> element
 */
const gml_edge = (r, m) => `<edge directed="true" source="${r.module.id}" target="${m.id}" />`;

/**
 * Make a GraphML <node> element containing a sub-graph (like a group) from mapping
 * of nodes_by_group and the group key string
 * @param  {Object} nodes_by_group : Mapping of group key to array of nodes
 * @param  {String} group          : Group key
 * @return {String}                : string representation of a GraphML <node> containing a sub-graph
 */
const gml_group = (nodes_by_group, group) => `
    <node id="${group}">
      <data key="d0"><![CDATA[${group}]]></data>
      <data key="d1">${nodes_by_group[group].aggregated_size/1024}</data>
      <data key="d2">${nodes_by_group[group].aggregated_size}</data>
      <graph id="${group}:" edgedefault="directed">
        ${nodes_by_group[group].join('\n        ')}
      </graph>
    </node>`;

const gml_document = (nodes, edges) => `<?xml version="1.0" encoding="UTF-8"?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns
     http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
  <key for="node" id="d0" attr.type="string" attr.name="name" />
  <key for="node" id="d1" attr.type="double" attr.name="sizekb" />
  <key for="node" id="d2" attr.type="int" attr.name="sizebytes" />
  <graph id="G" edgedefault="directed">
    ${nodes.join('\n    ')}
    ${edges.join('\n    ')}
  </graph>
</graphml>`;


/**
 * Webpack plugin which generates a GraphML file of the modules used in compilation,
 * and their dependency relationships.
 */
class ModuleGraphPlugin {
  /**
   * Construtor
   * @param  {Object} options : Optional configuration parameters;
   *                          .filename   {String}  : output filename
   *                          .use_groups {Boolean} : If true, output sub-graphs of packages, else do not group the nodes
   */
  constructor(options={}) {
    if (!options.filename) {
      throw new Error('ModuleGraphPlugin requires filename option');
    }
    this.filename = options.filename;
    this.use_groups = !!options.use_groups;
  }

  /**
   * Construct a GraphML document from the webpack compilation
   * @param  {Object} info : webpack compilation object
   * @return {String}      : String containing GraphML XML document
   */
  _makeGraphML(compilation) {
    const nodes_by_group = {};
    const edges = [];

    const cwd = process.cwd();

    // for each module in the compilation ...
    compilation.modules.forEach(
      m => {
        // ... figure out how to name it ...
        let resourceName = m.resource;
        if (!resourceName) {
          resourceName = m.portableId || '?? unknown'; // TOOO: better alternative ?
        }
        let [group, file] = group_name(cwd, resourceName);
        if (!this.use_groups) {
          file = path.join(group, file || '.');
        }

        // ... create a group for it ...
        if (!(group in nodes_by_group)) {
          nodes_by_group[group] = [];
        }
        // ... append to the group a GraphML node for it ...
        nodes_by_group[group].push(gml_node(m, file));
        // ... update the group's aggregated size ...
        nodes_by_group[group].aggregated_size = (nodes_by_group[group].aggregated_size || 0) + m.size();
        // ... and for each of the reasons the module was compiled ...
        m.reasons.forEach(
          // ... add an edge to the reason's module
          r => edges.push(gml_edge(r, m))
        );
      }
    );
    let nodes;
    // if we're outputting groups, then format each group into a sub-graph <node> element
    if (this.use_groups) {
      nodes = Object.keys(nodes_by_group).map(
        group => gml_group(nodes_by_group, group)
      );
    // else flatten all of the groups into a single list of <node> elements
    } else {
      nodes = Object.keys(nodes_by_group).reduce(
        (nodelist, group) => {
          return nodelist.concat(nodes_by_group[group]);
        },
        []
      );
    }
    // then return all the nodes and edges wrapped up in a GraphML document
    return gml_document(nodes, edges);
  };

  /**
   * Plugin entry function
   * @param  {Object} compiler : the webpack compiler object
   */
  apply(compiler) {
    /**
     * Attach the GraphML generation to the compiler 'done' step.
     * @param  {Object} compilation : the webpack compilation object
     * @param  {Function} callback  : plugin completion callback
     */
    compiler.plugin('emit', (compilation, callback) => {
      // Create the GraphML document from the compilation
      const graphML = this._makeGraphML(compilation);

      // and add it as a new asset in the compilation
      compilation.assets[this.filename] = {
        source() {
          return graphML;
        },
        size() {
          return graphML.length;
        }
      }

      callback();
    });
  };
};

module.exports = ModuleGraphPlugin;
